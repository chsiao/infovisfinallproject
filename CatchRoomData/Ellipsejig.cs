//
//////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2012 Autodesk, Inc.  All rights reserved.
//
//  Use of this software is subject to the terms of the Autodesk license 
//  agreement provided at the time of installation or download, or which 
//  otherwise accompanies this software in either electronic or hard copy form.   
//
//////////////////////////////////////////////////////////////////////////////

using System;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.ApplicationServices;
using System.Reflection;

using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Collections;
using System.Runtime.InteropServices;
using System.Diagnostics;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.GraphicsInterface;
using DrawingOjbect = Autodesk.AutoCAD.DatabaseServices;

using System.Data;
using System.Data.SQLite;

[assembly: CommandClass(typeof(JigSample.TestEntityJig))]


//This application implements a command called ellipsejig. It will help you 
//create an ellipse from scratch by doing a jig. The user is first asked to
//enter the ellipse major axis followed by ellipse minor axis. 

//To use ellipsejig.dll:
//1. Start AutoCAD and open a new drawing.
//2. Type netload and select ellipsejig.dll.
//3. Execute the ellipsejig command, defined by ellipsejig.dll.

//Please add the References acdbmgd.dll,acmgd.dll,
//Autodesk.AutoCAD.Interop.dll and Autodesk.AutoCAD.Interop.Common.dll
//before trying to build this project.

namespace JigSample
{
	public class TestEntityJig 
	{
		class EllipseJig : EntityJig	
		{
			Point3d mCenterPt,mAxisPt,acquiredPoint;
			Vector3d mNormal,mMajorAxis;
			double mRadiusRatio;
			
			int mPromptCounter;
			
			DynamicDimensionDataCollection m_dims;

			public EllipseJig(Point3d center,Vector3d vec):base(new Ellipse())
			{
				mCenterPt = center;
				mNormal = vec;
				mRadiusRatio = 0.00001;
				mPromptCounter = 0;

				m_dims = new DynamicDimensionDataCollection();
				Dimension dim1 = new AlignedDimension();
				dim1.SetDatabaseDefaults();
				m_dims.Add(new DynamicDimensionData(dim1,true,true));
                dim1.DynamicDimension = true;
				Dimension dim2 = new AlignedDimension();
				dim2.SetDatabaseDefaults();
				m_dims.Add(new DynamicDimensionData(dim2,true,true));
                dim2.DynamicDimension = true;
			
			}
	
			protected  override SamplerStatus Sampler(JigPrompts prompts)
			{
				JigPromptPointOptions jigOpts = new JigPromptPointOptions();
				jigOpts.UserInputControls = (UserInputControls.Accept3dCoordinates | UserInputControls.NoZeroResponseAccepted | UserInputControls.NoNegativeResponseAccepted);
			
				if(mPromptCounter == 0)
				{
					jigOpts.Message = "\nEllipse Major axis:";
					PromptPointResult dres = prompts.AcquirePoint(jigOpts);
					
					Point3d axisPointTemp = dres.Value;
					if(axisPointTemp != mAxisPt)
					{
						mAxisPt = axisPointTemp;
					}
					else
						return SamplerStatus.NoChange;

					if(dres.Status == PromptStatus.Cancel)
						return SamplerStatus.Cancel;
					else
						return SamplerStatus.OK;
					
					
				}
				else if (mPromptCounter == 1) 
				{
					jigOpts.BasePoint = mCenterPt;
					jigOpts.UseBasePoint = true;
					jigOpts.Message = "\nEllipse Minor axis:";
					double radiusRatioTemp = -1;
					PromptPointResult res = prompts.AcquirePoint(jigOpts);
					acquiredPoint = res.Value;
					radiusRatioTemp = mCenterPt.DistanceTo(acquiredPoint);

                    // Ensure the radiusRatio is kept within the expected range.
                    if (radiusRatioTemp > 1.0)
                        radiusRatioTemp = 1.0;
				
					if (radiusRatioTemp != mRadiusRatio)
						mRadiusRatio = radiusRatioTemp;
					else 
						return SamplerStatus.NoChange;

					if(res.Status == PromptStatus.Cancel)
						return SamplerStatus.Cancel;
					else
						return SamplerStatus.OK;
			
				}
				else
				{
					return SamplerStatus.NoChange;
				}
				
				
			}
			protected override bool Update()
			{
				double radiusRatio = 1.0;
				switch (mPromptCounter) 
				{
					case 0:
						// At this time, mAxis contains the value of one
						// endpoint of the desired major axis.  The
						// AcDbEllipse class stores the major axis as the
						// vector from the center point to where the axis
						// intersects the ellipse path (such as half of the true
						// major axis), so we already have what we need.
						//
						mMajorAxis = mAxisPt - mCenterPt;
						break;
					case 1:
						// Calculate the radius ratio.  mRadiusRatio
						// currently contains the distance from the ellipse
						// center to the current pointer position.  This is
						// half of the actual minor axis length.  Since
						// AcDbEllipse stores the major axis vector as the
						// vector from the center point to the ellipse curve
						// (half the major axis), to get the radius ratio we
						// simply divide the value currently in mRadiusRatio
						// by the length of the stored major axis vector.
						//
						
						radiusRatio = mRadiusRatio / mMajorAxis.Length;
						break;
				}
				
				try
				{
					((Ellipse)Entity).Set(mCenterPt,new Vector3d(0,0,1),mMajorAxis,radiusRatio,0.0,6.28318530717958647692);
					UpdateDimensions();
					
				}
				catch(System.Exception)
				{
					return false;  
				}
				
				return true;  

			}
			protected override DynamicDimensionDataCollection GetDynamicDimensionData(double dimScale)
			{
				return m_dims;
			}
			protected override void OnDimensionValueChanged(Autodesk.AutoCAD.DatabaseServices.DynamicDimensionChangedEventArgs e)
			{
				
			}
			void UpdateDimensions()
			{
				if(mPromptCounter == 0)
				{
					Ellipse myellipse = (Ellipse)Entity;
					AlignedDimension dim = (AlignedDimension)m_dims[0].Dimension;
					dim.XLine1Point = myellipse.Center;
					dim.XLine2Point = mAxisPt;
					dim.DimLinePoint = myellipse.Center;
				}
				else
				{
					Ellipse myellipse = (Ellipse)Entity;
					AlignedDimension dim2 = (AlignedDimension)m_dims[1].Dimension;
					dim2.XLine1Point = myellipse.Center;
					dim2.XLine2Point = acquiredPoint;
					dim2.DimLinePoint = myellipse.Center;
				}

			}
			public void setPromptCounter(int i)
			{
				mPromptCounter = i;
			}
			public Entity GetEntity()
			{
				return Entity;
			}

		}
		[CommandMethod("ellipsejig")]
		static public void DoIt()
		{
			Editor ed = Application.DocumentManager.MdiActiveDocument.Editor;   
			PromptPointOptions opts = new PromptPointOptions("\nEnter Ellipse Center Point:");
			PromptPointResult res = ed.GetPoint(opts);

			Vector3d x = Application.DocumentManager.MdiActiveDocument.Database.Ucsxdir;
			Vector3d y = Application.DocumentManager.MdiActiveDocument.Database.Ucsydir;
			Vector3d NormalVec = x.CrossProduct(y);
		

			Database db = Application.DocumentManager.MdiActiveDocument.Database;
			Autodesk.AutoCAD.DatabaseServices.TransactionManager tm = db.TransactionManager;
		
			//Create Ellipsejig
			EllipseJig jig = new EllipseJig(res.Value,NormalVec.GetNormal());
			//first call drag to get the major axis
			jig.setPromptCounter(0);
			Application.DocumentManager.MdiActiveDocument.Editor.Drag(jig);
			// Again call drag to get minor axis					
			jig.setPromptCounter(1);
			Application.DocumentManager.MdiActiveDocument.Editor.Drag(jig);

			//Append entity.
			using (Transaction myT = tm.StartTransaction())
			{              
				BlockTable bt = (BlockTable)tm.GetObject(db.BlockTableId,OpenMode.ForRead,false);
				BlockTableRecord btr = (BlockTableRecord)tm.GetObject(bt[BlockTableRecord.ModelSpace],OpenMode.ForWrite,false);
				btr.AppendEntity(jig.GetEntity());
				tm.AddNewlyCreatedDBObject(jig.GetEntity(),true);
				myT.Commit();
			}

				
		}

        public static string layerSpaceName = "K_TXT_SPC_U";
        public static string layerSpaceArea = "Q_TXT_SPC";
        public static string layerNursingUnitDistance = "DistanceLine";
        public static string layerWindows = "Window";
        public static string layerDoor = "Door";
        public static string layerSpace = "LNK_SPC_UA";

        [CommandMethod("GetData")]
        static public void GetData()
        {
            //string connectionString = @"Data Source=K:\Repositories\infovisfinallproject\Database\newdata.db3";
            //SQLiteConnection sqlConnection = new SQLiteConnection(connectionString);
            //sqlConnection.Open();
            //string commandText = @"SELECT Name FROM Room";
            //SQLiteCommand command = new SQLiteCommand(commandText, sqlConnection);
            //SQLiteDataReader reader = command.ExecuteReader();

            //System.Data.DataTable dtRooms = new System.Data.DataTable();
            //dtRooms.Load(reader);
            

            Document dc = Application.DocumentManager.MdiActiveDocument;
            Database db = dc.Database;
            Editor ed = dc.Editor;

            Transaction tr =
            db.TransactionManager.StartTransaction();


            TypedValue[] filterList = new TypedValue[8]{
                    new TypedValue((int)DxfCode.Operator,"<or"),
                    new TypedValue((int)DxfCode.LayerName, layerSpaceName),
                    new TypedValue((int)DxfCode.LayerName, layerSpaceArea),
                    new TypedValue((int)DxfCode.LayerName, layerNursingUnitDistance),
                    new TypedValue((int)DxfCode.LayerName, layerWindows),
                    new TypedValue((int)DxfCode.LayerName, layerDoor),
                    new TypedValue((int)DxfCode.LayerName, layerSpace),
                    new TypedValue((int)DxfCode.Operator,"or>")
                };

            SelectionFilter filter = new SelectionFilter(filterList);
            PromptSelectionResult selRes = ed.SelectAll(filter);
            ObjectId[] ids = selRes.Value.GetObjectIds();

            using (tr)
            {                

                //LayerTable lt = (LayerTable)tr.GetObject(db.LayerTableId, OpenMode.ForRead);
                //foreach (ObjectId lyId in lt)
                //{
                //    LayerTableRecord layer = (LayerTableRecord)tr.GetObject(lyId, OpenMode.ForRead);

                //}
                List<DBObject> lsRoomName = new List<DBObject>();
                List<DBObject> lsRoomArea = new List<DBObject>();
                List<DrawingOjbect.Polyline> plinesSpace = new List<DrawingOjbect.Polyline>();
                List<DrawingOjbect.Polyline> plinesDistance = new List<DrawingOjbect.Polyline>();
                List<DrawingOjbect.Line> linesWindows = new List<Line>();
                List<DrawingOjbect.Line> linesDoors = new List<Line>();

                foreach (ObjectId selID in ids)
                {
                    DBObject obj = tr.GetObject(selID, OpenMode.ForRead);                   



                    if (obj is DBText)
                    {
                        DBText text = (DBText)obj;
                        if (text.Layer == layerSpaceName)
                        {
                            // check if the room is in the database
                            lsRoomName.Add(text);
                        }
                        else if (text.Layer == layerSpaceArea)
                        {
                            if (text.TextString.Contains("PATIENT"))
                                lsRoomArea.Add(text);
                        }
                    }
                    else if (obj is DrawingOjbect.MText)
                    {
                        MText text = (MText)obj;
                        if (text.Layer == layerSpaceName)
                        {
                            // check if the room is in the database
                            lsRoomName.Add(text);
                        }
                        else if (text.Layer == layerSpaceArea)
                        {
                            if (text.Text.Contains("PATIENT") || text.Text.Contains("NURSE"))
                                lsRoomArea.Add(text);
                        }
                    }
                    else if (obj is DrawingOjbect.Polyline)
                    {
                        DrawingOjbect.Polyline pl = (DrawingOjbect.Polyline)obj;
                        if (pl.Layer == layerSpace)
                        {
                            plinesSpace.Add(pl);
                        }
                        else if (pl.Layer == layerNursingUnitDistance)
                        {
                            plinesDistance.Add(pl);
                        }
                    }
                    else if (obj is DrawingOjbect.Line)
                    {
                        DrawingOjbect.Line line = (DrawingOjbect.Line)obj;
                        if (line.Layer == layerDoor)
                        {
                            linesDoors.Add(line);
                        }
                        else if (line.Layer == layerWindows)
                        {
                            linesWindows.Add(line);
                        }
                    }
                }


                List<Room> validRooms = new List<Room>();
                foreach(DrawingOjbect.Polyline pline in plinesSpace)
                {
                    Room newRoom = ConstructPatientRoom(pline,lsRoomName, lsRoomArea, linesWindows, linesDoors, plinesDistance);
                    if (newRoom != null) validRooms.Add(newRoom);
                }


                PrintCSVFile(validRooms);
            }
        }

        private static void PrintCSVFile(List<Room> rooms)
        {

            using (StreamWriter sw = new StreamWriter(@"K:\Repositories\infovisfinallproject\Database\RawData\roomdata.csv"))
            {
                StringBuilder sb = new StringBuilder();
                string firstRow = "Room Name,Room Type,Angle,Orientation,Area,Window Width,Door Width,Distance To Station,";
                sb.AppendLine(firstRow);

                foreach (Room rm in rooms)
                {
                    sb.AppendLine(rm.ToString());    
                }

                sw.Write(sb.ToString());
            }
        }

        private static Room ConstructPatientRoom(DrawingOjbect.Polyline spacePath, List<DBObject> roomname, List<DBObject> roomarea, 
            List<DrawingOjbect.Line> windows, List<DrawingOjbect.Line> doors, List<DrawingOjbect.Polyline> plinesDistance)
        {            
            // convert the space polyline into a region            
            List<System.Drawing.PointF> pathpts = new List<System.Drawing.PointF>();
            for (int i = 0; ; i++)
            {
                try
                {
                    Point3d pt = spacePath.GetPoint3dAt(i);
                    pathpts.Add(new System.Drawing.PointF((float)pt.X, (float)pt.Y));
                }
                catch (System.Exception e)
                {
                    break;
                }                
            }
           
            System.Drawing.Drawing2D.GraphicsPath path = new System.Drawing.Drawing2D.GraphicsPath();
            path.AddPolygon(pathpts.ToArray());            

            System.Drawing.Region region = new System.Drawing.Region(path);


            Room room = new Room();

            #region try to get teh room area text and convert it to the real area            
            foreach (DBObject dbo in roomarea)
            {
                System.Drawing.PointF pt = new System.Drawing.PointF();
                string text = "";

                if (dbo is DBText)
                {
                    DBText dbtext = (DBText)dbo;
                    pt = new System.Drawing.PointF((float)dbtext.Position.X, (float)dbtext.Position.Y);
                    text = dbtext.TextString;
                }
                else if (dbo is DrawingOjbect.MText)
                {
                    MText mtext = (MText)dbo;
                    pt = new System.Drawing.PointF((float)mtext.Location.X, (float)mtext.Location.Y);
                    text = mtext.Text;
                }

                if (region.IsVisible(pt))
                {
                    if (text.ToLower().Contains("patient"))
                        room.RoomType = "Patient Room";
                    else if(text.ToLower().Contains("nurs"))
                        room.RoomType = "Nurse Station";

                    //PATIENT ROOM\r\n164.11 SqFt
                    text = text.Remove(0, text.IndexOf('\n') + 1);
                    text = text.Remove(text.IndexOf('S'));

                    double area = 0;
                    if (double.TryParse(text, out area))
                    {
                        area = area * 0.092903;
                        room.Area = area;
                    }                                        

                    break;
                }
            }
            #endregion

            try
            {
                if (room.Area == null || room.Area == 0)
                    return null;
            }
            catch (System.Exception e)
            {

            }


            #region try to get the room name            
            foreach (DBObject dbo in roomname)
            {
                System.Drawing.PointF pt = new System.Drawing.PointF();
                string text = "";

                if (dbo is DBText)
                {
                    DBText dbtext = (DBText)dbo;
                    pt = new System.Drawing.PointF((float)dbtext.Position.X, (float)dbtext.Position.Y);
                    text = dbtext.TextString;
                }
                else if (dbo is DrawingOjbect.MText)
                {
                    MText mtext = (MText)dbo;
                    pt = new System.Drawing.PointF((float)mtext.Location.X, (float)mtext.Location.Y);
                    text = mtext.Text;
                }

                if (region.IsVisible(pt))
                {
                    //ACADText cadtext = new ACADText();
                    //cadtext.Text = text;
                    //cadtext.Location = pt;

                    room.RoomNameLocation = pt;
                    room.RoomNameText = text;
                    break;
                }
            }
            #endregion

            #region try to get the windows

            //System.Diagnostics.Debug.WriteLine("room name:" + room.RoomNameText);
            double minDist = double.MaxValue;            
            foreach (DrawingOjbect.Line winLine in windows)
            {
                Point3d ctrWin = new Point3d((winLine.StartPoint.X + winLine.EndPoint.X) / 2, (winLine.StartPoint.Y + winLine.EndPoint.Y) / 2, (winLine.StartPoint.Z + winLine.EndPoint.Z) / 2);
                Point3d closetPt = spacePath.GetClosestPointTo(ctrWin, false);
                double distance = ctrWin.DistanceTo(closetPt);

                if (distance < 10 && distance < minDist)
                {
                    minDist = distance;
                    room.WinWidth = winLine.Length * 25.4;
                    room.WinLocation = new System.Drawing.PointF((float)closetPt.X, (float)closetPt.Y);
                }
            }
            #endregion

            #region try to get the doors
            //System.Diagnostics.Debug.WriteLine("room name:" + room.RoomNameText);
            minDist = double.MaxValue;
            foreach (DrawingOjbect.Line doorLine in doors)
            {
                Point3d ctrDoor = new Point3d((doorLine.StartPoint.X + doorLine.EndPoint.X) / 2, (doorLine.StartPoint.Y + doorLine.EndPoint.Y) / 2, (doorLine.StartPoint.Z + doorLine.EndPoint.Z) / 2);
                Point3d closetPt = spacePath.GetClosestPointTo(ctrDoor, false);
                double distance = ctrDoor.DistanceTo(closetPt);

                if (distance < 10 && distance < minDist)
                {
                    minDist = distance;
                    room.DoorWidth = doorLine.Length * 25.4;
                    room.DoorLocation = new System.Drawing.PointF((float)closetPt.X, (float)closetPt.Y);
                }
            }
            #endregion

            #region Find the corresponding distance line and calucate the distance
                       
            foreach (DrawingOjbect.Polyline plineDist in plinesDistance)
            {
                Point3d doorctrPt = new Point3d(room.DoorLocation.X, room.DoorLocation.Y, plineDist.StartPoint.Z);
                Point3d closetPt = plineDist.GetClosestPointTo(doorctrPt, false);
                double distance = doorctrPt.DistanceTo(closetPt);

                System.Diagnostics.Debug.WriteLine(distance);
                if (distance < 100)
                {                   
                    // calculate the distance from closet point to the end of the polyline,
                    // and decide the which end point is close to the nursing station
                    room.DistanceToStation = (distance + plineDist.GetDistAtPoint(closetPt))*25.4; 

                    break;
                }
            }

            #endregion

            if (room.DoorLocation != null && room.WinLocation != null && room.WinLocation.X != 0)
            {
                Vector2d vec = new Vector2d(room.WinLocation.X - room.DoorLocation.X, room.WinLocation.Y - room.DoorLocation.Y);
                double angle = vec.Angle * 180 / Math.PI;
                
                room.Angle = angle;
                
                if ((angle >= -45 && angle < 45) || (angle >= 315 && angle < 405)) room.Orientation = "East";
                else if (angle >= 45 && angle < 135) room.Orientation = "North";
                else if (angle >= 135 && angle < 225) room.Orientation = "West";
                else if (angle >= 225 && angle < 315) room.Orientation = "South";
 
            }

            return room;
        }        

    }

    public class Room
    {
        public Room()
        {

        }

        public string RoomNameText { get; set; }
        public string RoomType { get; set; }
        public string Orientation { get; set; }
        public double Angle { get; set; }
        public System.Drawing.PointF RoomNameLocation { get; set; }        
        //public List<Line> Windows { get; set; }
        //public DrawingOjbect.Polyline Spaces { get; set; }
        //public List<Line> Doors { get; set; }

        public double? Area { get; set; }
        public double WinWidth { get; set; }
        public double DoorWidth { get; set; }
        public double DistanceToStation { get; set; }
        public System.Drawing.PointF DoorLocation { get; set; }
        public System.Drawing.PointF WinLocation { get; set; }

        public override string ToString()
        {
            return RoomNameText + "," + RoomType + "," + Angle + "," + Orientation + "," + Area.ToString() + "," + WinWidth.ToString() + "," + DoorWidth.ToString() + "," + DistanceToStation + ",";
        }
    }

    public struct ACADText
    {
        public System.Drawing.PointF Location { get; set; }
        public string Text { get; set; }
    }

}
