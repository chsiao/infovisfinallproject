<?php
  try
  {
    //open the database
    $pdo = new PDO('sqlite:newdata.db3');   
    $query = "SELECT Discharge, AverageScore FROM Overall_University_2";

    $statement = $pdo->prepare($query);
    $statement->execute();
    $results = $statement->fetchAll(PDO::FETCH_ASSOC);

    // return the results in json format
    echo json_encode($results);

    // close the database connection
    $db = NULL;
  }
  catch(PDOException $e)
  {
    print 'Exception : '.$e->getMessage();
  }
?>