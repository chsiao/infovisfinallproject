<?php
  try
  {

    $hospital = $_GET["hospital"];
    $floor = $_GET["floor"];
    $section = $_GET["section"];
    $question_id = $_GET["question"];

    $table = "" . $hospital . $floor;

    //open the database
    $db = new PDO('sqlite:newdata.db3');

    $query = 
      "SELECT 
        strftime('%m %Y', DischargeFrom) AS Discharge,
        AVG(Score) as AverageScore
      FROM 
        '$table'
      WHERE
        Question = (SELECT Question FROM SurveyQuestions WHERE rowid = '$question_id')
      GROUP BY 
        Discharge
      ORDER BY
        strftime('%Y', DischargeFrom)
      ";

    $sql = $db->prepare($query);
    $sql->execute();

    $results = $sql->fetchAll(PDO::FETCH_ASSOC);

    // return the results in json format
    echo json_encode($results);

    // close the database connection
    $db = NULL;
  }
  catch(PDOException $e)
  {
    print 'Exception : '.$e->getMessage();
  }
?>