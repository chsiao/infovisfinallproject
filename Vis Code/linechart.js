var lineColors = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf'];
var lineCount = -1;
var margin = {top: 10, right: 10, bottom: 100, left: 40},
    margin2 = {top: 10, right: 10, bottom: 20, left: 40},
    width = 960 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom,
    height2 = 100 - margin2.top - margin2.bottom;

var parseDate = d3.time.format("%b %Y").parse;

var parseDateJSON = d3.time.format("%m %Y").parse;

var x = d3.time.scale().range([0, width]),
    x2 = d3.time.scale().range([0, width]),
    y = d3.scale.linear().range([height, 0]),
    y2 = d3.scale.linear().range([height2, 0]);

var xAxis = d3.svg.axis().scale(x).orient("bottom"),
    xAxis2 = d3.svg.axis().scale(x2).orient("bottom"),
    yAxis = d3.svg.axis().scale(y).orient("left");

var brush = d3.svg.brush()
    .x(x2)
    .on("brush", brush)
    .on("brushend", brushend);

var line = d3.svg.line()
    .x(function(d) { return x(d.date); })
    .y(function(d) { return y(d.price); });

var line2 = d3.svg.line()
    .x(function(d) { return x2(d.date); })
    .y(function(d) { return y2(d.price); });

var area = d3.svg.area()
    .interpolate("monotone")
    .x(function(d) { return x(d.date); })
    .y0(height)
    .y1(function(d) { return y(d.price); });

var area2 = d3.svg.area()
    .interpolate("monotone")
    .x(function(d) { return x2(d.date); })
    .y0(height2)
    .y1(function(d) { return y2(d.price); });

var svg = d3.select("#test").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom);

svg.append("defs").append("clipPath")
    .attr("id", "clip")
    .append("rect")
    .attr("width", width)
    .attr("height", height);

var focus = svg.append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

var context = svg.append("g")
    .attr("transform", "translate(" + margin2.left + "," + margin2.top + ")");

var json = "realquery.php?hospital=Univ&floor=9&section=Admission&question=1";

var floor = 8;
var hospital = "Univ";
var section = "Admission";
var question = "Admission";
var beginDate = "2000-01-01";
var endDate = "2012-01-11";


d3.json(json, function(error, data) {

  data.forEach(function(d) {
    d.date = parseDateJSON(d.Discharge);
    d.price = +d.AverageScore;
  });

  x.domain(d3.extent(data.map(function(d) { return d.date; })));
  y.domain([0, 100]);
  x2.domain(x.domain());
  y2.domain(y.domain());

  context.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height2 + ")")
      .call(xAxis2);

  context.append("g")
      .attr("class", "x brush")
      .call(brush)
      .selectAll("rect")
      .attr("y", -6)
      .attr("height", height2 + 7);
});

function brush() {
  x.domain(brush.empty() ? x2.domain() : brush.extent());
  focus.select("path").attr("d", area);
  focus.select(".x.axis").call(xAxis);
}

function brushend() {
  var format = d3.time.format("%Y-%m-%d");
  beginDate = format(brush.extent()[0]);
  endDate = format(brush.extent()[1]);
  treejson = "roomquery.php?floor=" + floor + "&question=" + question + "&startdate=" + beginDate + "&enddate=" + endDate;
  //treejson = "roomscorequery.php?floor=" + floor + "&question=" + question + "&datestart=" + beginDate + "&dateend=" + endDate;

  updateScores(treejson);
}

function updateTree(jsonData) {
  d3.json(jsonData, function(error, data) {
    root = data;
    root.fixed = true;
    root.x = width / 2;
    root.y = height / 2;

    nodes = flatten(root);
    update();
  });
}

function addData()
{
  hospital = $('#hospital').val();
  floor = $('#floor').val();
  section = $('#section').val();
  questionNum = $('#question').val();
  question = $('#question :selected').text();

  json = "realquery.php?hospital=" + hospital + "&floor=" + floor + "&section=" + section + "&question=" + questionNum;
  treejson = "roomquery.php?floor=" + floor + "&question=" + question;

  d3.json(treejson, function(error, data) {     
    data.fixed = true;
    data.x = width / 2;
    data.y = height / 2;

    if(!nodes)
      nodes = flatten(data);
    else
    {
      var newnodes = flatten(data);
      newnodes.forEach(function(d) {
        nodes.push(d);
      });
    }

    update();
  });
  

  d3.json(json, function(error, data) {

    data.forEach(function(d) {
      d.date = parseDateJSON(d.Discharge);
      d.price = +d.AverageScore;
    });

    context.append("path")
        .datum(data)        
        .attr("class", "line")
        .attr("d", line2)
        .style("stroke", function(d)
          {            
            lineCount++;
            return lineColors[lineCount];
          });
  });
}       .attr("d", line);

    context.select("path")
        .datum(data)
        .attr("class", "line")
        .attr("d", line2);
  });
}

function updateData() {

  hospital = $('#hospital').val();
  floor = $('#floor').val();
  section = $('#section').val();
  questionNum = $('#question').val();
  question = $('#question :selected').text();
  json = "realquery.php?hospital=" + hospital + "&floor=" + floor + "&section=" + section + "&question=" + questionNum;
  treejson = "roomquery.php?floor=" + floor + "&question=" + question;

  updateTree(treejson);

  d3.json(json, function(error, data) {

    data.forEach(function(d) {
      d.date = parseDateJSON(d.Discharge);
      d.price = +d.AverageScore;
    });

    focus.select("path")
        .datum(data)
        .attr("class", "line")
        .attr("clip-path", "url(#clip)")
        .attr("d", line);

    context.select("path")
        .datum(data)
        .attr("class", "line")
        .attr("d", line2);
  });
}