<?php
  try
  {
    $section = $_GET["section"];//$_REQUEST["section"];

    //open the database
    $db = new PDO('sqlite:newdata.db3');

    $query = 
      "SELECT rowid, * FROM SurveyQuestions WHERE SectionId = '$section'";

    $sql = $db->prepare($query);
    $sql->execute();

    $option = "";

    while($row = $sql->fetch()) {
      $id = $row['rowid'];
      $name = $row['Question'];
      $option.="<option value=".$id.">".$name."</option>";
    }

    echo $option;

    // close the database connection
    $db = NULL;
  }
  catch(PDOException $e)
  {
    print 'Exception : '.$e->getMessage();
  }
?>