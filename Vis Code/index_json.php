<html>
<head>
  <meta charset="utf-8">
  <link href="tooltip.css" rel="stylesheet" type="text/css">
  <script type="text/javascript" src="jquery-1.8.2.js"></script>
  <script type="text/javascript" src="http://d3js.org/d3.v2.min.js"></script>
  <script src="http://d3js.org/d3.v3.min.js"></script>
  <script src="nvtooltip.js"></script>

  <script type="text/javascript" src="d3.layout.cloud.js"></script> 

  <script type="text/javascript">
    $(document).ready(function() {
      $('#section').load('sections.php');
      $('#question').load('questions.php?section=Admission');
    });

    function changeSection(section) {
      var xmlhttp;
        if (window.XMLHttpRequest)
          {// code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp=new XMLHttpRequest();
          }
        else
          {// code for IE6, IE5
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
          }
        xmlhttp.onreadystatechange=function()
        {
          if (xmlhttp.readyState==4 && xmlhttp.status==200)
            {
              var res=xmlhttp.responseText;
              document.getElementById("question").innerHTML=res;
            }
        }
      xmlhttp.open("GET","questions.php?section="+section,true);
      xmlhttp.send();
    }
  </script>

  <style>

    body {
      font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
      margin: auto;
      position: relative;
      width: 960px;
    }

    text {
      font: 10px sans-serif;
    }

    form {
      position: absolute;
      right: 10px;
      top: 10px;
    }

    svg {
      font: 10px sans-serif;
    }

/*    path {
      fill: steelblue;
    }*/

    #boundary {      
        position:relative;
        top: 10;
        left: -200;
        width: 960px;
        height: 550px;
        background-color: #FFF8F1;
        border:2px solid green;
        -webkit-border-radius: 15px;
        -webkit-border-bottom-right-radius: 10px;
        -moz-border-radius: 15px;
        -moz-border-radius-bottomright: 10px;
        border-radius: 15px;
        border-bottom-right-radius: 10px;
    }

    #boundary2 {      
        position:relative;
        top: -520;
        left: -200;
        width: 960px;
        height: 150px;
        background-color: #FFF8F1;
        border:2px solid green;
        -webkit-border-radius: 15px;
        -webkit-border-bottom-right-radius: 10px;
        -moz-border-radius: 15px;
        -moz-border-radius-bottomright: 10px;
        border-radius: 15px;
        border-bottom-right-radius: 10px;
    }

    #sideWindow{
        position:relative;
        display: inline-block;
        width: 300px;
        height: 725px;
        top: -1247;
        left:530;
        background-color: #FFF8F1;
        border:2px solid green;
        -webkit-border-radius: 15px;
        -webkit-border-bottom-right-radius: 10px;
        -moz-border-radius: 15px;
        -moz-border-radius-bottomright: 10px;
        border-radius: 15px;
        border-bottom-right-radius: 10px;
    }

    #chart{
        position:relative;
        top: -550;
        left:-200;
        display: inline-block;
        width: 960px;
        height: 550px;
    }

    #test{
        position:relative;
        top: -1320;
        left:-210;
        display: inline-block;
        width: 250px;
        height: 50px;
    }

    #navBar{
        position:relative;
        top: 10;
        left:-200;
        display: inline-block;
        width: 970px;
        height: 50px;
    }

    #add{
        position:relative;
        z-index:2;
        top: 12;
        left:00;
        display: inline-block;
        width: 100px;
        height: 50px;
    }

    #titleBar{
        position:relative;
        z-index:2;
        top:-1930;
        left:230;
        display: inline-block;
        width: 250px;
        height: 50px;
        /*background-color: green;*/
    }

    #wordCloud{
        position:relative;
        z-index:2;
        top:-1955;
        left:820;
        display: inline-block;
        width: 250px;
        height: 700px;
    }

    .axis path,
    .axis line {
      fill: none;
      stroke: #000;
      shape-rendering: crispEdges;
    }

    .line {
      fill: none;
      stroke: steelblue;
      stroke-width: 1.5 px;
    }

    .brush .extent {
      stroke: #fff;
      fill-opacity: .125;
      shape-rendering: crispEdges;
    }

    circle.node {
      cursor: pointer;
      stroke: #000;
      stroke-width: .5px;
    }

    line.link {
      fill: none;
      stroke: #9ecae1;
      stroke-width: 1.5px;
    }

  </style>
</head>

  <body>
    <div id ="navBar">
      <form>
        <select name="hospital" id="hospital">
          <option value="Univ">Emory University Hospital</option>
          <option value="Midtown">Emory University Hospital Midtown</option>
        </select>
        <select name="floor" id="floor" > <!-- onChange="updateData()" -->
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
          <option value="7">7</option>
          <option value="8">8</option>
          <option value="9">9</option>
          <option value="10">10</option>
          <option value="11">11</option>
        </select>
        <select name="section" id="section" onChange="changeSection(this.value)"> <!--  -->
        </select>
        <select name="question" id="question" > <!-- onChange="updateData()" -->
            <option value=""></option>
        </select>
      </form>
      <div id="add">
        <button onclick="addData()">Add</button>
        <!-- <button onclick="switchMode()">Switch</button> -->
      </div>
    </div>
    <div id="boundary"></div>
    <div id="chart"></div>
    <div id="boundary2"></div>
    <div id="test"></div>
    <div id="sideWindow"></div>
    <div id="titleBar"><h2 id="curWordCloud"></h2></div>
    <div id="wordCloud"></div>

    <script type="text/javascript" src="linechart.js"></script>
    <script type="text/javascript" src="treeview.js"></script>

    <script>
  


</script>   

  </body>
</html>