<?php
  try
  {
    //open the database
    $db = new PDO('sqlite:newdata.db3');

    $query = 
      "SELECT * FROM SurveySections";

    $sql = $db->prepare($query);
    $sql->execute();

    while($row = $sql->fetch()) {
      $id = $row['SectionId'];
      $name = $row['Section'];
      echo("<option value=$id>$name</option>");
    }

    // close the database connection
    $db = NULL;
  }
  catch(PDOException $e)
  {
    print 'Exception : '.$e->getMessage();
  }
?>