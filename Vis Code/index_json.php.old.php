<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <link href="tooltip.css" rel="stylesheet" type="text/css">
  <script type="text/javascript" src="jquery-1.8.2.js"></script>
  <script src="http://d3js.org/d3.v3.min.js"></script>
  <script src="nvtooltip.js"></script>

  <script type="text/javascript">
    $(document).ready(function() {
      $('#section').load('sections.php');
      $('#question').load('questions.php?section=Admission');
    });

    function changeSection(section) {
      var xmlhttp;
        if (window.XMLHttpRequest)
          {// code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp=new XMLHttpRequest();
          }
        else
          {// code for IE6, IE5
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
          }
        xmlhttp.onreadystatechange=function()
        {
          if (xmlhttp.readyState==4 && xmlhttp.status==200)
            {
              var res=xmlhttp.responseText;
              document.getElementById("question").innerHTML=res;
            }
        }
      xmlhttp.open("GET","questions.php?section="+section,true);
      xmlhttp.send();
    }
  </script>

  <style>

    body {
      font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
      margin: auto;
      position: relative;
      width: 960px;
    }

    text {
      font: 10px sans-serif;
    }

    form {
      position: absolute;
      right: 10px;
      top: 10px;
    }

    svg {
      font: 10px sans-serif;
    }

    path {
      fill: steelblue;
    }

    .axis path,
    .axis line {
      fill: none;
      stroke: #000;
      shape-rendering: crispEdges;
    }

    .line {
      fill: none;
      stroke: steelblue;
      stroke-width: 1.5px;
    }

    .brush .extent {
      stroke: #fff;
      fill-opacity: .125;
      shape-rendering: crispEdges;
    }

    circle.node {
      cursor: pointer;
      stroke: #000;
      stroke-width: .5px;
    }

    line.link {
      fill: none;
      stroke: #9ecae1;
      stroke-width: 1.5px;
    }

  </style>
</head>

  <body>
    <div>
      <form>
        <select name="hospital" id="hospital">
          <option value="Univ">Emory University Hospital</option>
          <option value="Midtown">Emory University Hospital Midtown</option>
        </select>
        <select name="floor" id="floor" onChange="updateData()">
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
          <option value="7">7</option>
          <option value="8">8</option>
          <option value="9">9</option>
          <option value="10">10</option>
          <option value="11">11</option>
        </select>
        <select name="section" id="section" onChange="changeSection(this.value)">
        </select>
        <select name="question" id="question" onChange="updateData()">
            <option value=""></option>
        </select>
      </form>
      <button onclick="updateData()">Change</button>
    </div>
    
    <div id="chart"></div>
    <div id="test"></div>

    <script type="text/javascript" src="linechart.js"></script>
    <script type="text/javascript" src="treeview.js"></script>
  </body>
</html>