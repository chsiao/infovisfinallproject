var width = 960,
    height = 550,
    nodes = [],
    mode = 0,
    node,
    link,
    root;

var force = d3.layout.force()
    .on("tick", tick)
    .gravity(0)
    .charge(0)
    .linkDistance(
      function(d) { 
        var dist = 170;

        if(!d.target._children)
        {
          if (d.target.distBtwStation == null)
          {
            if(!d.target.children)
              dist = 30;
            else
              dist = 170;
          }
          else if(d.target.distBtwStation == 0)
            dist = 30;
          else
          {
            dist = d.target.distBtwStation/350;
            //console.log(d.target.distBtwStation);
          }
        }

        return dist; 
      })
    .size([width, height]);

var vis = d3.select("#chart").append("svg")
    .attr("width", width)
    .attr("height", height);

var wordScale=d3.scale.linear().domain([1,10]).range([10,80]).clamp(true);
var wordColor=d3.scale.linear().domain([10,20,40,80]).range(["blue","green","orange","red"]);

var wordCloud = d3.layout.cloud().size([250, 500])
    .rotate(0)
    .fontSize(function(d) { return wordScale(d.size); })
    .on("end", draw);
var wordviz = d3.select("#wordCloud").append("svg")
    .attr("width", 250)
    .attr("height", 500)

var tooltip = d3.select("body")
  .append("div")
  .style("position", "absolute")
  .style("z-index", "10")
  .style("visibility", "hidden")
  .text("a simple tooltip");

var mouseoverTheNode = function(e)
{
  var winwidth = !e.windowWidth ? "-":Math.round(parseFloat(e.windowWidth)*100)/100000;
  var doorwidth = !e.doorWidth ? "-":Math.round(parseFloat(e.doorWidth)*100)/100000;
  var angle = !e.angle ? "-":Math.round(e.angle*100)/100;
  var area = !e.size ? "-":e.size.length > 30? "-" : Math.round(e.size*1000)/1000;
  var score = !e.score ? "-":e.score;
  var name = !e.name ? "-":e.name;


  var content = '<h2>' + name + '</h2>' +
                '<p>' +
                '<span class="value"> Score: ' + score + '</span></br>'+
                '<span class="value"> Area: ' + area + ' sq m</span></br>'+
                '<span class="value"> Window Width: ' + winwidth + ' m</span></br>'+
                '<span class="value"> Door Width: ' + doorwidth + ' m</span></br>'+
                '<span class="value"> Orientation: ' + angle + ' degree</span></br>'+              
                '</p>';

  nvtooltip.show([event.pageX-230, event.pageY + 10], content);
}

var mousemoveTheNode = function()
{
}

var mouseoutTheNode = function(e)
{
  nvtooltip.cleanup();  
}

function update() {
  
  var links = d3.layout.tree().links(nodes);

  // Restart the force layout.
  force
      .nodes(nodes)
      .links(links)
      .start();

  // Update the links…
  link = vis.selectAll("line.link")
      .data(links, function(d) { return d.target.id; });

  // Enter any new links.
  link.enter().insert("line", ".node")
      .attr("class", "link")
      .attr("x1", function(d) { return d.source.x; })
      .attr("y1", function(d) { return d.source.y; })
      .attr("x2", function(d) { return d.target.x; })
      .attr("y2", function(d) { return d.target.y; });

  // Exit any old links.
  link.exit().remove();

  // Update the nodes…
  node = vis.selectAll("circle.node")
      .data(nodes, function(d) { return d.id; });
      //.style("fill", color);

  node.transition()
      .attr("r", function(d) {
        var size = 4.5;
        if(!d.children)
        {
          if(d.size == 0)
            size = 4.5;
          else
            size = d.size/2;
        }
        return size; 
      }); //Math.sqrt(d.size) / 10

  // Enter any new nodes.
  node.enter().append("circle")
      .attr("class", "node")
      .attr("cx", function(d) { return d.x; })
      .attr("cy", function(d) { return d.y; })
      .attr("r", function(d) { 
        var size = 4.5;
        if(!d.children)
        {
          if(d.size == 0)
            size = 4.5;
          else
            size = d.size/2;
        }
        return size; 
      })
      .style("fill", color)
      .on("click", click)
      .on("mouseover", mouseoverTheNode)
      .on("mousemove", mousemoveTheNode)
      .on("mouseout", mouseoutTheNode)
      .call(force.drag);

  // Exit any old nodes.
  node.exit().remove();
}

function tick() {
  link.attr("x1", function(d) {      
      return d.source.x; 
    })
      .attr("y1", function(d) {       
      return d.source.y; 
    })
      .attr("x2", function(d) 
      {
      
      if(d.target.angle && d.target.distBtwStation && d.target.distBtwStation > 0)
      {
        var dist = d.target.distBtwStation / 350;
        var rad = -d.target.angle * Math.PI / 180;
        var xt = d.source.x + dist * Math.cos(rad);        
        d.target.x = xt;
        //console.log(xt);  
      }

      return d.target.x; 
    })
      .attr("y2", function(d) { 
      
      if(d.target.angle && d.target.distBtwStation && d.target.distBtwStation > 0)
      {
        var dist = d.target.distBtwStation / 350;
        var rad = -d.target.angle * Math.PI / 180;
        var yt = d.source.y + dist * Math.sin(rad); 
        d.target.y = yt;
        //console.log(yt);
      }

      return d.target.y; 
    });  

  node //each(collide(.5))
    .attr("cx", function(d) { return d.x; })
    .attr("cy", function(d) { return d.y; });
}

// Resolve collisions between nodes.
function collide(alpha) {
  var quadtree = d3.geom.quadtree(nodes);
  return function(d) {
    var r = d.size + padding,
        nx1 = d.x - r,
        nx2 = d.x + r,
        ny1 = d.y - r,
        ny2 = d.y + r;
    quadtree.visit(function(quad, x1, y1, x2, y2) {
      if (quad.point && (quad.point !== d)) {
        var x = d.x - quad.point.x,
            y = d.y - quad.point.y,
            l = Math.sqrt(x * x + y * y),
            r = d.size + quad.point.size;
        if (l < r) {
          l = (l - r) / l * alpha;
          d.x -= x *= l;
          d.y -= y *= l;
          quad.point.x += x;
          quad.point.y += y;
        }
      }
      return x1 > nx2
          || x2 < nx1
          || y1 > ny2
          || y2 < ny1;
    });
  };
}

// Color leaf nodes orange, and packages white or blue.
// add the color logics here:
function color(d) {
  
  var nodecolor = "#A0A2A4";
  
  if(d.children)
    var nodecolor = d._children ? "#3182bd" : d.children ? lineColors[lineCount] : "#fd8d3c";
  else if(d.score > 0)
  {
    var scaledColor = d3.scale.linear()
      .domain([60, 80, 100])
      .range(["red", "white", "green"]);
    nodecolor = scaledColor(d.score);
  }
  
  return nodecolor;
}

// Shows the word cloud on the side panel
function click(d) {

  if(d.children)
  {
    // Todo: collapse the child from the node
    // d._children = d.children;    
    // d.children = null;
    // update();
  }
  else
  {    
    // showing the word clouds in the side bar
    var commentjson = "commentQuery.php?room=" + d.name + "&startdate=" + beginDate + "&enddate=" + endDate;
    d3.json(commentjson, function(error, topic) {  
      
      wordCloud
          .words(topic)        
          .start();    
    })

    document.getElementById('curWordCloud').innerHTML = d.name;
  }
}

function draw(words) {
        
        wordviz.selectAll("text").remove();          

        wordviz.append("g")          
          .attr("transform", "translate(120,300)")
          .selectAll("text")
          .data(words)
          .enter().append("text")
          .style("font-size", function(d) { return d.size + "px"; })
          .style("fill", function(d) { return wordColor(d.size); })
          .style("opacity", .75)
          .attr("text-anchor", "middle")
          .attr("transform", function(d) {
             return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
           })
          .text(function(d) { return d.text; });
          
        wordviz.selectAll("text")
          .on("mouseover", mouseoverWordCloud)
          .on("mouseout", mouseoutWordCloud);

    }

var mouseoverWordCloud = function(e)
{  
  var keyword = e.text;
  var comments = "";
  var x = e.x;
  var y = e.y;

   e.comments.forEach(function(d)
        {
          comments = comments + '<li>' + d + '</li></br>'
        });

  var content = '<h2>' + keyword + '</h2>' +                
                 '<ul style="width:800px;">' +
                 comments +        
                 '</ul>';

  nvtooltip.show([x, y], content, 'c');
}

var mouseoutWordCloud = function(e)
{
  nvtooltip.cleanup();  
}

// Returns a list of all nodes under the root.
function flatten(root) {
  var tmpnodes = [], i = 0;

  function recurse(node) {
    if (node.children) {
      node.size = node.children.reduce
        (function(p, v) 
          { 
            return p + recurse(v);
             }, 0);
    }

    if (!node.id) node.id = ++i;

    //if(node.name.length < 6)
    tmpnodes.push(node);    

    return node.size;
  }

  root.size = recurse(root);
  return tmpnodes;
}

function updateScores(jsondata)
{
  var tmpdata;

  d3.json(jsondata, function(error, data) {
    tmpdata = flatten(data);


    node.style("fill", function(d)
    {
      var nodecolor = "#A0A2A4"

      if(!d.children)
      {
        tmpdata.forEach(function(newd)
        {
          if(d.name == newd.name)
          {
            d.score = newd.score;

            if(d.score != 0)
            {

              var scaledColor = d3.scale.linear()
                .domain([60, 80, 100])
                .range(["red", "white", "green"]);
                      
              nodecolor = scaledColor(d.score);
            }            
          }
        });        
      }

      return nodecolor;
    });

  });
}

function switchMode()
{


}