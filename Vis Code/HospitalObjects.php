<?php
  class Floor
  {
    public $name = "";
    public $nursingUnit = array();

    public function __construct()
    {
    }
  }

  class NursingUnit
  {
    public $name = "";
    public $patientRooms = array();
    
    public function __construct()
    {
    }
  }

  class PatientRoom
  {
    public $name = '';
    public $size = 0.0;
    public $numBeds = 0;   
    public $windowWidth = 0.0;
    public $distBtwStation = 0.0;
    public $doorWidth = 0.0;
    public $orientation = '';
    public $angle = '';
    public $score = 0;

    public function __construct(){}
  }
?>