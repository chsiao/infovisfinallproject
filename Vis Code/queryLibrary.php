<?php

	//date format example: 2006-01-01
	function RoomScores($floornum, $roomname, $datestart, $dateend, $questionname)
	{	
		$table = "Univ".$floornum;		
		try
		{				
			$pdo = new PDO('sqlite:newdata.db3');
			$query = "SELECT     			
		     			AVG(".$table.".Score) AS AverageScore
					FROM 
					    ".$table."			    
					WHERE
					    ".$table.".Room = '".$roomname."'
					And
					    ".$table.".Question = '".$questionname."'
					AND
						".$table.".DischargeTo BETWEEN '".$datestart."' AND '".$dateend."'
					";

//echo $query;
		    $statement = $pdo->prepare($query);
		    $statement->execute();	
		    $results = $statement->fetchAll(PDO::FETCH_ASSOC);

		    //strftime('%m %Y', Univ5.DischargeTo) AS Discharge,
			return $results;

		}
		catch(PdoException $e)
		{
		    print 'Exception : '.$e->getMessage();
		}
	}


	//date format example: 2006-01-01
	function FloorScores($floornum, $datestart, $dateend, $questionname)
	{	
		$table = "Univ".$floornum;		
		try
		{				
			$pdo = new PDO('sqlite:newdata.db3');
			$query = "SELECT
						".$table.".Room,
		     			AVG(".$table.".Score) AS AverageScore
					FROM 
					    ".$table."
					WHERE					    
					    ".$table.".Question = '".$questionname."'
					AND
						".$table.".DischargeTo BETWEEN '".$datestart."' AND '".$dateend."'
					GROUP BY 
        				".$table.".Room
					";

//echo $query;
		    $statement = $pdo->prepare($query);
		    $statement->execute();	
		    $results = $statement->fetchAll(PDO::FETCH_ASSOC);
		    
			return $results;

		}
		catch(PdoException $e)
		{
		    print 'Exception : '.$e->getMessage();
		}
	}


	function Comments($datestart, $dateend, $roomname)
	{		
		try
		{				
			$pdo = new PDO('sqlite:newdata.db3');
			$stlength = strlen($roomname);
			$extendRoomname = "";
			if(stlength	< 5)
			{
				$extendroomname = $roomname."01";
			}else
			{
				$extendroomname = substr($roomname, 0,-1);
			}

			$query = "SELECT
						Comments.Comments,
					    Comments.Keywords				    
					FROM
					     Comments 
					INNER JOIN 
					     (SELECT IT_Room, id, DischargeDate FROM SURVEY) as SurveyRoom
					ON 
					     SurveyRoom.id = Comments.Survey
					Where
					     (SurveyRoom.IT_Room = '".$roomname."' OR SurveyRoom.IT_Room = '".$extendroomname."')
					AND
					     SurveyRoom.DischargeDate BETWEEN '".$datestart."' AND '".$dateend."'
					ORDER BY 
					     SurveyRoom.DischargeDate
					";

			//echo $query;
		    $statement = $pdo->prepare($query);
		    $statement->execute();	
		    $results = $statement->fetchAll(PDO::FETCH_ASSOC);
		    
		    // post processing for getting the right data structures
		    $finalresults = array();		    
		    $finalstring = "";
			foreach ($results as $oneroom) 
    		{ 
    			if($oneroom["Comments.Keywords"] != "")
    			{
    				$keyword = $oneroom["Comments.Keywords"];
    				$keywords = explode(",", $keyword);
    				//var_dump($keywords);
    				//echo " ";
    				foreach($keywords as $onekeyword)
    				{
	    				$hasentry = 0;
	    				$count = count($finalresults);

    					for($i=0;$i<$count;$i++)
    					{
    						//echo $finalresults[$i]->text." and ". $onekeyword;
    						if($finalresults[$i]->text == $onekeyword)
    						{
    							if(!in_array($oneroom["Comments.Comments"], $finalresults[$i]->comments))
    							{
	    							$finalresults[$i]->size = $finalresults[$i]->size + 1;
	    							array_push($finalresults[$i]->comments, $oneroom["Comments.Comments"]);	    							
    							}
    							$hasentry = 1;    							
	    						break;
    						}
    						//echo " is false; ";
    					}

    					if($hasentry == 0)
    					{    	
    						//echo " Adding a new array; ";					
							$oneentry = new SurveyComment();
							$oneentry->text = $onekeyword;
							$oneentry->size = 1;
							array_push($oneentry->comments, $oneroom["Comments.Comments"]);

							array_push($finalresults, $oneentry);
						}
					}
    			}
    		}

			return $finalresults;
		}
		catch(PdoException $e)
		{
		    print 'Exception : '.$e->getMessage();
		}
	}


class SurveyComment
{
	public $text = '';
    public $size = 0;
    public $comments = array();
}


