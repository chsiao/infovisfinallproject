<?php
include 'queryLibrary.php';
include 'HospitalObjects.php';

  try
  {
    $floornum = $_GET['floor'];
    if($floornum == "") 
      $floornum = 5; 
    $datestart = $_GET['startdate'];
    if($datestart == "")
      $datestart = "2006-01-01";
    $dateend = $_GET['enddate'];
    if($dateend == "")
      $dateend = "2012-09-01";
    $question = $_GET['question'];
    if($question == "")
      $question = "Tests and Treatments"; 

    $pdo = new PDO('sqlite:newdata.db3');
    $query = "SELECT 
            *
          From
            NursingUnit
          INNER JOIN 
            (SELECT * FROM Room) as UnitRooms
            ON
            NursingUnit.Name = UnitRooms.IT_Unit
          Where
            NursingUnit.Site = 'EMORY UNIVERSITY HOSPITAL'
          And
            NursingUnit.FloorNum = ".$floornum."
          ORDER BY 
            NursingUnit.Name
          ";


    $statement = $pdo->prepare($query);
    $statement->execute();
    $results = $statement->fetchAll(PDO::FETCH_ASSOC);

    $currentUnitName = "";
    $EntireFloor = new Floor();
    $EntireFloor->name = "Floor" . $floornum;
    $newUnit = new NursingUnit();    

    foreach ($results as $oneroom) 
    {      
      if($oneroom["IT_Unit"] != $currentUnitName && $currentUnitName != "")
      {        
        $newUnit->name = $currentUnitName;    
        array_push($EntireFloor->nursingUnit, $newUnit);
        $newUnit = new NursingUnit();                    
      }    
      $currentUnitName = $oneroom["IT_Unit"];

      // try to query the scores and add it into the $PatientRoom
      $score = RoomScores($floornum, $oneroom["Name"], $datestart, $dateend, $question);

      $PatientRoom = new PatientRoom();
      $PatientRoom->name = $oneroom["Name"];
      $PatientRoom->size = $oneroom["Area"];
      $PatientRoom->score = $score[0]["AverageScore"];
      $PatientRoom->numBeds = $oneroom["NumBeds"];      
      $PatientRoom->windowWidth = $oneroom["WindowWidth"];
      $PatientRoom->distBtwStation = $oneroom["DistBtwStation"];
      $PatientRoom->doorWidth = $oneroom["DoorWidth"];
      $PatientRoom->orientation = $oneroom["Orientation"];
      $PatientRoom->angle = $oneroom["Angle"];
      
      if(strlen($PatientRoom->name) < 6)
        array_push($newUnit->patientRooms, $PatientRoom);
    }
    
    // push the last one
    $newUnit->name = $currentUnitName;    
    array_push($EntireFloor->nursingUnit, $newUnit);
      
    // return the results in json format
    $jsonMessage = json_encode($EntireFloor);    

    $search = array("nursingUnit", "patientRooms");
    $replaceTo = array("children","children");
    $jsonMessage = str_replace($search, $replaceTo, $jsonMessage);

    echo $jsonMessage;

    // close the database connection
    $db = NULL;
  }
  catch(PDOException $e)
  {
    print 'Exception : '.$e->getMessage();
  }

?>