﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using Ribbon = Microsoft.Office.Tools.Ribbon;
using Excel = Microsoft.Office.Interop.Excel;
using System.Data.SQLite;


namespace ConvertToSQLite
{
    public partial class Ribbon1
    {
        string[] directories = {
                                    @"K:\Repositories\infovisfinallproject\Database\RawData\UniversityRatings\Admission",
                                    @"K:\Repositories\infovisfinallproject\Database\RawData\UniversityRatings\Discharge",
                                    @"K:\Repositories\infovisfinallproject\Database\RawData\UniversityRatings\Meals",
                                    @"K:\Repositories\infovisfinallproject\Database\RawData\UniversityRatings\Nurses",
                                    @"K:\Repositories\infovisfinallproject\Database\RawData\UniversityRatings\Overall Assessment",
                                    @"K:\Repositories\infovisfinallproject\Database\RawData\UniversityRatings\Personal Issues",
                                    @"K:\Repositories\infovisfinallproject\Database\RawData\UniversityRatings\Physician",
                                    @"K:\Repositories\infovisfinallproject\Database\RawData\UniversityRatings\Room",
                                    @"K:\Repositories\infovisfinallproject\Database\RawData\UniversityRatings\Tests and Treatments",
                                    @"K:\Repositories\infovisfinallproject\Database\RawData\UniversityRatings\Visitors and Family"
                               };

        string RoomDataDir = @"K:\repositories\infovisfinallproject\Database\RawData\UniversityRoomData";

        private void Ribbon1_Load(object sender, Ribbon.RibbonUIEventArgs e)
        {

        }

        private void btCommentConvert_Click(object sender, Ribbon.RibbonControlEventArgs e)
        {
            OpenFileDialog saveDialog = new OpenFileDialog();
            saveDialog.Filter = "SQLite3 File (*.db3)|*.db3";

            int maxrow = 10000;
            if (this.MaxRow.Text == "" || !int.TryParse(this.MaxRow.Text, out maxrow))
            {
                MessageBox.Show("Please enter a number");
                return;
            }

            if (saveDialog.ShowDialog() == DialogResult.OK)
            {
                // Put the filename in a more canonical format
                String fullpath = saveDialog.FileName.ToLower();

                //SQLiteConnection.CreateFile(fullpath);
                

                string connectionString = @"Data Source=" + Path.GetFullPath(fullpath);
                SQLiteConnection connection = new SQLiteConnection(connectionString);

                connection.Open();
                //createCommentTables(connection);

                List<OneRow> OneSheet = new List<OneRow>();

                Excel._Worksheet sheet = GlobalVariable.WorkBook.ActiveSheet;
                for (int i = 2; i <= maxrow; i++)
                {
                    List<object> survey = new List<object>();
                    List<object> patient = new List<object>();
                    List<object> room = new List<object>();
                    List<object> comment = new List<object>();

                    OneRow aRow = new OneRow();
                    aRow.Survey = survey;
                    aRow.Patient = patient;
                    aRow.Room = room;
                    aRow.Comments = comment;

                    OneSheet.Add(aRow);

                    for (int j = 1; j <= 50; j++)
                    {
                        Excel.Range cell = sheet.Cells[i, j];
                        object value = cell.Value;
                        if (value != null && value.ToString().Contains('\''))
                        {                            
                            value = AddSingleQuoteIntoString(value.ToString());
                        }

                        switch (j)
                        {
                            case 1:  // 0: keyword
                            case 6:  // 1: section
                            case 8:  // 2: rating
                            case 9:  // 3: comments
                                comment.Add(value);
                                break;
                            case 2:  // 0: patient name
                            case 3:  // 1: phone
                            case 24: // 2: Sex

                                // if there is no phone number stored, generate a number randomly
                                if (j == 3 && (value == null || value.ToString() == ""))
                                {
                                    Random rn = new Random();
                                    int rnNum = rn.Next(100, 1000000);
                                    value = rnNum.ToString();
                                }

                                patient.Add(value);
                                break;
                            case 5:  // 0: site
                            case 13: // 1: unit
                            case 31: // 2: it room
                            case 35: // 3: it unit
                                room.Add(value);
                                break;
                            case 7:  // 0: survey
                            case 10: // 1: Received Date
                            case 11: // 2: Discharge Date
                            case 12: // 3: Specialty
                            case 14: // 4: emergency
                            case 15: // 5: admision unexpected
                            case 16: // 6: roomate
                            case 17: // 7: special diet
                            case 18: // 8: LifeSupportOption
                            case 19: // 9: organ donation
                            case 20: // 10: patients bill of rights
                            case 21: // 11: insurance has limits
                            case 22: // 12: type of insurance
                            case 23: // 13: days in hospital
                            case 25: // 14: age
                            case 26: // 15: health rating
                            case 27: // 16: reason
                            case 28: // 17: language
                            case 29: // 18: first stay here
                            case 30: // 19: it discharge
                            case 34: // 20: it admit date
                            case 43: // 21: rate entire hospital
                                if (value != null && (j == 14 || j == 15 || j == 16 || j == 17 || j == 18 || j == 19 || j == 20 || j == 21))
                                {
                                    if (value.ToString() == "Yes")
                                        value = 1;
                                    else if (value.ToString() == "No")
                                        value = 0;
                                }

                                if (j == 10 || j == 11 || j == 19 || j == 20)
                                {
                                    if(value != null && value.ToString().Contains('/'))
                                    {
                                        string datatimeString = convertDateTime(value.ToString());
                                        value = datatimeString;
                                    }
                                }

                                survey.Add(value);
                                break;
                        }
                    }

                    saveCommentValuesIntoTable(connection, aRow);
                }


                System.Media.SoundPlayer simpleSound = new System.Media.SoundPlayer(@"c:\Windows\Media\chimes.wav");
                simpleSound.Play();

                connection.Close();
            }
        }

        private void btRoom_Click(object sender, Ribbon.RibbonControlEventArgs e)
        {
            OpenFileDialog saveDialog = new OpenFileDialog();
            saveDialog.Filter = "SQLite3 File (*.db3)|*.db3";

            int maxrow = 100000;

            if (saveDialog.ShowDialog() == DialogResult.OK)
            {
                String fullpath = saveDialog.FileName.ToLower();

                string connectionString = @"Data Source=" + Path.GetFullPath(fullpath);
                SQLiteConnection connection = new SQLiteConnection(connectionString);

                connection.Open();
                SQLiteCommand updateCommand = CreateUpdateCommand(connection);

                foreach (string f in Directory.GetFiles(RoomDataDir))
                {
                    if (f.Contains(".DS_Store")) continue;
                    GlobalVariable.Application.Workbooks.Open(f);                    
                    int sheetcount = 1;
                    foreach (Excel._Worksheet sheet in GlobalVariable.WorkBook.Sheets)
                    {
                        sheetcount++;

                        bool iscontinue = false;
                        for (int i = 2; i <= maxrow; i++)
                        {

                            for (int j = 1; j <= 8; j++)
                            {
                                Excel.Range cell = sheet.Cells[i, j];
                                object value = cell.Value;

                                if (value == null)
                                {
                                    iscontinue = true;
                                    break;
                                }

                                switch(j)
                                {
                                    case 1:
                                        updateCommand.Parameters["@Name"].Value = value.ToString();
                                        break;
                                    case 2:
                                        if (value.ToString() == "Nurse Station")
                                        {
                                            iscontinue = true;
                                        }
                                        break;
                                    case 3:
                                        updateCommand.Parameters["@Angle"].Value = double.Parse(value.ToString()) - 27;
                                        break;
                                    case 4:
                                        updateCommand.Parameters["@Orient"].Value = value;
                                        break;
                                    case 5:
                                        updateCommand.Parameters["@Area"].Value = double.Parse(value.ToString());
                                        break;
                                    case 6:
                                        updateCommand.Parameters["@WinWidth"].Value = double.Parse(value.ToString());
                                        break;
                                    case 7:
                                        updateCommand.Parameters["@DoorWidth"].Value = double.Parse(value.ToString());
                                        break;
                                    case 8:
                                        updateCommand.Parameters["@Distance"].Value = double.Parse(value.ToString());
                                        break;
                                }

                                if (iscontinue)
                                    break;
                            }

                            if (iscontinue)
                            {
                                iscontinue = false; // this is not a patient room
                                continue;
                            }

                            updateCommand.ExecuteNonQuery();
                        }

                    }

                }
            }
        }

        private SQLiteCommand CreateUpdateCommand(SQLiteConnection conn)
        {
            SQLiteCommand cmd = new SQLiteCommand("UPDATE Room SET Area = @Area, WindowWidth = @WinWidth, DistBtwStation = @Distance, DoorWidth = @DoorWidth, Orientation = @Orient, Angle = @Angle WHERE Name = @Name", conn);

            cmd.Parameters.Add(new SQLiteParameter("@Area"));
            cmd.Parameters.Add(new SQLiteParameter("@WinWidth"));
            cmd.Parameters.Add(new SQLiteParameter("@Distance"));
            cmd.Parameters.Add(new SQLiteParameter("@DoorWidth"));
            cmd.Parameters.Add(new SQLiteParameter("@Orient"));
            cmd.Parameters.Add(new SQLiteParameter("@Angle"));
            cmd.Parameters.Add(new SQLiteParameter("@Name"));

            return cmd;
        }

        private void btConvertScore_Click(object sender, Ribbon.RibbonControlEventArgs e)
        {
            OpenFileDialog saveDialog = new OpenFileDialog();
            saveDialog.Filter = "SQLite3 File (*.db3)|*.db3";

            int maxrow = 100000;
            //if (this.MaxRow.Text == "" || !int.TryParse(this.MaxRow.Text, out maxrow))
            //{
            //    MessageBox.Show("Please enter a number.");
            //    return;
            //}

            if (this.txtSection.Text == "")
            {
                //MessageBox.Show("Please enter a Section Name.");
                //return;
            }

            if (saveDialog.ShowDialog() == DialogResult.OK)
            {
                String fullpath = saveDialog.FileName.ToLower();

                string connectionString = @"Data Source=" + Path.GetFullPath(fullpath);
                SQLiteConnection connection = new SQLiteConnection(connectionString);

                connection.Open();
                //createTables(connection);

                foreach (string directory in directories)
                {
                    foreach (string f in Directory.GetFiles(directory))
                    {
                        if (f.Contains(".DS_Store")) continue;
                        GlobalVariable.Application.Workbooks.Open(f);
                        string section = directory.Remove(0, directory.LastIndexOf('\\') + 1);

                        int sheetcount = 1;
                        foreach (Excel._Worksheet sheet in GlobalVariable.WorkBook.Sheets)
                        {
                            sheetcount++;
                            createRatingTables(connection);
                            bool iscontinue = false;
                            for (int i = 11; i <= maxrow; i++)
                            {
                                lbCurRow.Label = i.ToString();
                                List<object> oneRow = new List<object>();
                                
                                for (int j = 1; j <= 5; j++)
                                {
                                    Excel.Range cell = sheet.Cells[i, j];
                                    object value = cell.Value;

                                    if (value == null)
                                    {
                                        iscontinue = true;
                                        break;
                                    }

                                    if (j != 2)
                                    {
                                        if (j == 3)
                                        {
                                            string stringvalue = value.ToString();
                                            if (stringvalue.Contains('\''))
                                                value = AddSingleQuoteIntoString(stringvalue);
                                        }
                                        else if (j == 5 && value.ToString() == "0")
                                        {
                                            iscontinue = true;
                                            break;
                                        }

                                        oneRow.Add(value);
                                    }
                                    else
                                    {
                                        string tmpValue = value.ToString();

                                        if (tmpValue == "'Total'")
                                        {
                                            oneRow.Add("");
                                            oneRow.Add("");
                                        }
                                        else
                                        {
                                            string[] tmpValues = tmpValue.Split(' ');
                                            string value1 = convertDateTime(tmpValues[0].Remove(0, 1));
                                            value1 = value1.Insert(0, "20");
                                            string value2 = convertDateTime(tmpValues[1].Remove(tmpValues[1].Length - 1));
                                            value2 = value2.Insert(0, "20");
                                            oneRow.Add(value1);
                                            oneRow.Add(value2);
                                        }
                                    }
                                }

                                if (iscontinue)
                                {
                                    iscontinue = false;
                                    continue;
                                }

                                SQLiteCommand command = connection.CreateCommand();
                                string commandText = @"INSERT INTO Scores(Room, DischargeFrom, DischargeTo, Section, Question, Score, NumOfSurveys)
                                    VALUES(" + oneRow[0] + ",'" + oneRow[1] + "','" + oneRow[2] + "','" + section + "','" + oneRow[3] + "','" + oneRow[4] + "','" + oneRow[5] + "')";
                                command = new SQLiteCommand(commandText, connection);
                                command.ExecuteNonQuery();
                            }
                        }

                        System.Media.SoundPlayer simpleSound = new System.Media.SoundPlayer(@"c:\Windows\Media\chimes.wav");
                        simpleSound.Play();

                        GlobalVariable.WorkBook.Close();
                        //GlobalVariable.Application.ThisWorkbook.Close();
                    }
                }
                    connection.Close();                
            }
        }

        private void saveCommentValuesIntoTable(SQLiteConnection connection, OneRow oneRow)
        {
            try
            {
                SQLiteCommand command = connection.CreateCommand();

                // insert into nursing unit table if the nursing unit is not exist
                string commandText = @"INSERT OR IGNORE INTO NursingUnit(Name, Site)
                              VALUES('" + oneRow.Room[3] + "','" + oneRow.Room[0] + "')";
                command = new SQLiteCommand(commandText, connection);
                command.ExecuteNonQuery();


                // insert into room table if the room is not exist
                commandText = @"INSERT OR IGNORE INTO Room(Name, Site, IT_Unit, Unit)
                              VALUES('" + oneRow.Room[2] + "','" + oneRow.Room[0] + "','" + oneRow.Room[3] + "','" + oneRow.Room[1] + "')";
                command = new SQLiteCommand(commandText, connection);
                command.ExecuteNonQuery();


                // insert into patient table if the patient is not exist
                commandText = @"INSERT OR IGNORE INTO Patient(Name, Phone, Sex)
                              VALUES('" + oneRow.Patient[0] + "','" + oneRow.Patient[1] + "','" + oneRow.Patient[2] + "')";
                command = new SQLiteCommand(commandText, connection);
                command.ExecuteNonQuery();


                // insert into survey table if the survey is not exist
                commandText = @"INSERT OR IGNORE INTO Survey(Patient, id, ReceivedDate, DischargeDate, IT_DischargeDate, IT_AdmitDate, 
                                                         Specialty, IT_Room, IsEmergency, IsUnexpected, HasRoomate, IsSpecialDiet,
                                                         IsExpLifeSupOpt, WantOrganDonate, HasBillRights, HasIsuranceLimits, TypeInsurance,
                                                         DaysInHospital, PatientAge, HealthRating, ReasonChoosingHospital, Language, RateHospital)
                              VALUES('" + oneRow.Patient[1] + "','" + oneRow.Survey[0] + "','" + oneRow.Survey[1] + "','" + oneRow.Survey[2] + "','" + oneRow.Survey[19] + "','" + oneRow.Survey[20] +
                                         "','" + oneRow.Survey[3] + "','" + oneRow.Room[2] + "','" + oneRow.Survey[4] + "','" + oneRow.Survey[5] + "','" + oneRow.Survey[6] + "','" + oneRow.Survey[7] +
                                         "','" + oneRow.Survey[8] + "','" + oneRow.Survey[9] + "','" + oneRow.Survey[10] + "','" + oneRow.Survey[11] + "','" + oneRow.Survey[12] +
                                         "','" + oneRow.Survey[13] + "','" + oneRow.Survey[14] + "','" + oneRow.Survey[15] + "','" + oneRow.Survey[16] + "','" + oneRow.Survey[17] + "','" + oneRow.Survey[21] + "')";
                command = new SQLiteCommand(commandText, connection);
                command.ExecuteNonQuery();


                // insert into Comment table if the comment is not exist
                commandText = @"INSERT INTO Comments(Survey, Section, Rating, Comments, Keywords)
                              VALUES('" + oneRow.Survey[0] + "','" + oneRow.Comments[1] + "','" + oneRow.Comments[2] + "','" + oneRow.Comments[3] + "','" + oneRow.Comments[0] + "')";
                command = new SQLiteCommand(commandText, connection);
                command.ExecuteNonQuery();

                command.Dispose();
            }
            catch (Exception ex)
            {
                System.Media.SoundPlayer simpleSound = new System.Media.SoundPlayer(@"c:\Windows\Media\Alarm10.wav");
                simpleSound.Play();
            }
        }

        private void createCommentTables(SQLiteConnection connection)
        {
            SQLiteCommand command = connection.CreateCommand();

            #region create the table for Site Enum
            string commandText = @"CREATE TABLE IF NOT EXISTS Site(
                Type    CHAR(30)       PRIMARY KEY NOT NULL,
                Seq     INTEGER
            )";
            command.CommandText = commandText;
            command.ExecuteNonQuery();

            commandText = @"INSERT INTO Site(Type, Seq) VALUES ('Emory University Hospital',1), ('Emory Midtown Hospital',2), ('Saint Joseph Hospital',3)";
            command.CommandText = commandText;
            command.ExecuteNonQuery();

            #endregion

            #region create the UNIT Layout Type for Patient Room
            commandText = @"CREATE TABLE IF NOT EXISTS UnitLayoutType(
                Type    CHAR(30)       PRIMARY KEY NOT NULL,
                Seq     INTEGER
            )";
            command.CommandText = commandText;
            command.ExecuteNonQuery();

            commandText = @"INSERT INTO UnitLayoutType(Type, Seq) VALUES ('Traingle',1), ('Single Load',2), ('Double Loaded Corridor',3), ('Single Loaded Corridor',4)";
            command.CommandText = commandText;
            command.ExecuteNonQuery();
            #endregion

            #region create the UNIT Layout Type for Patient Room
            commandText = @"CREATE TABLE IF NOT EXISTS UnitType(
                Type    CHAR(30)       PRIMARY KEY NOT NULL,
                Seq     INTEGER
            )";
            command.CommandText = commandText;
            command.ExecuteNonQuery();

            commandText = @"INSERT INTO UnitType(Type, Seq) VALUES ('NotDefined',1), ('NICU',2)";
            command.CommandText = commandText;
            command.ExecuteNonQuery();
            #endregion

            #region create the Orientation Type for Patient Room
            commandText = @"CREATE TABLE IF NOT EXISTS Orientation(
                Orientation     CHAR(5)       PRIMARY KEY NOT NULL,
                Seq             INTEGER
            )";
            command.CommandText = commandText;
            command.ExecuteNonQuery();

            commandText = @"INSERT INTO Orientation(Orientation, Seq) VALUES ('West',1), ('East',2), ('North',3),('South',4)";
            command.CommandText = commandText;
            command.ExecuteNonQuery();
            #endregion

            #region Create Nursing Station table
            commandText = @"CREATE TABLE IF NOT EXISTS NursingStation(
                Name        CHAR(20)        PRIMARY KEY NOT NULL,                
                Area        REAL,
                NumOfNurse  INTEGER                 
            )";
            command.CommandText = commandText;
            command.ExecuteNonQuery();
            #endregion

            #region Create Nursing Unit table
            commandText = @"CREATE TABLE IF NOT EXISTS NursingUnit(
                Name        CHAR(10)        PRIMARY KEY NOT NULL,
                Site        CHAR(30)        NOT NULL DEFAULT ('Emory University Hospital') REFERENCES Site(Type),
                Station     BLOB,           -- there could be multiple station in a nursing unit
                UnitLayout  CHAR(30)        REFERENCES UnitLayoutType(Type),
                UnitType    CHAR(30)        NOT NULL DEFAULT ('NotDefined') REFERENCES UnitType(Type),
                FloorNum    INTEGER
            )";
            command.CommandText = commandText;
            command.ExecuteNonQuery();
            #endregion

            #region Create room table
            commandText = @"CREATE TABLE IF NOT EXISTS Room(
                Name            CHAR(20)       PRIMARY KEY NOT NULL,
                Site            CHAR(30)       NOT NULL DEFAULT ('Emory University Hospital') REFERENCES Site(Type),
                IT_Unit         CHAR(10)       REFERENCES NursingUnit(Name),
                Unit            CHAR(20),
                NumBeds         INTEGER        NOT NULL DEFAULT 1,
                HasBath         BOOL           NOT NULL DEFAULT 1,
                Area            REAL,
                WindowArea      REAL,     
                Type            CHAR(30),
                DistBtwStation  BLOB           -- use the dictionary => StationName/Distance,
                Orientation     CHAR(5)        REFERENCES Orientation(Orientation),
                HandWash        TEXT
            )";
            command.CommandText = commandText;
            command.ExecuteNonQuery();
            #endregion

            #region Create Patient table
            commandText = @"CREATE TABLE IF NOT EXISTS Patient(
                Name            CHAR(200),       
                Phone           CHAR(20)    PRIMARY KEY NOT NULL,
                Sex             CHAR(10),
                BirthDate       DATETIME               
            )";
            command.CommandText = commandText;
            command.ExecuteNonQuery();
            #endregion

            #region Create Survey table
            commandText = @"CREATE TABLE IF NOT EXISTS SURVEY(
                id                  INTEGER                 PRIMARY KEY,       
                Patient             CHAR(20)                REFERENCES Patient(Phone),
                ReceivedDate        DATETIME,
                DischargeDate       DATETIME,
                IT_DischargeDate    DATETIME,
                IT_AdmitDate        DATETIME,
                Specialty           CHAR(50),
                IT_Room             CHAR(20)                REFERENCES Room(Name),
                Room                CHAR(20),
                IsEmergency         BOOL,
                IsUnexpected        BOOL,
                HasRoomate          BOOL,
                IsSpecialDiet       BOOL,
                IsExpLifeSupOpt     BOOL,
                WantOrganDonate     BOOL,
                HasBillRights       BOOL,
                HasIsuranceLimits   BOOL,
                TypeInsurance       TEXT,
                DaysInHospital      INTEGER,
                PatientAge          INTEGER,                -- patients could admit the hospital multiple times
                HealthRating        CHAR(10),
                ReasonChoosingHospital  TEXT,
                Language            CHAR(20),
                RateHospital        INTEGER
            )";
            command.CommandText = commandText;
            command.ExecuteNonQuery();
            #endregion

            #region Create Section Enum table            
            commandText = @"CREATE TABLE IF NOT EXISTS Section(
                Seq              INTEGER,       
                Section          CHAR(20)                PRIMARY KEY NOT NULL
            )";
            command.CommandText = commandText;
            command.ExecuteNonQuery();

            commandText = @"INSERT INTO Section(Section, Seq) VALUES ('Admission',1), ('Visitors and Family',2), ('Physician',3),('Room',4),('Meals',5), ('Tests and Treatments',6), ('Discharge',7), ('Personal Issues', 8), ('Overall Assessment', 9), ('Intensive/Critical Care', 10), ('Comm w/ Nurses', 11), ('Response of Hosp Staff', 12), ('Hospital Environment', 13), ('Pain Management', 14), ('Comm About Medicines', 15), ('About You', 16), ('Comments', 17), ('Others', 18)";
            command.CommandText = commandText;
            command.ExecuteNonQuery();
            #endregion

            #region Create Rating Enum table
            commandText = @"CREATE TABLE IF NOT EXISTS Rating(
                Seq              INTEGER,       
                Rating           CHAR(10)                PRIMARY KEY NOT NULL DEFAULT ('NotDefined')
            )";
            command.CommandText = commandText;
            command.ExecuteNonQuery();

            commandText = @"INSERT INTO Rating(Rating, Seq) VALUES ('Positive',1), ('Negative',2), ('Open',3), ('Mixed',4), ('NotDefined', 5)";
            command.CommandText = commandText;
            command.ExecuteNonQuery();
            #endregion

            #region create a comment table
            commandText = @"CREATE TABLE IF NOT EXISTS Comments(
                                    id              INTEGER     PRIMARY KEY,                                    
                                    Survey          INTEGER     REFERENCES SURVEY(id),
                                    Section         CHAR(20)    REFERENCES Section(Section),
                                    Rating          CHAR(10)    REFERENCES Rating(Rating),
                                    Comments        TEXT,
                                    Keywords        TEXT
                                    )";

            command.CommandText = commandText;
            command.ExecuteNonQuery();
            #endregion

            command.Dispose();
        }        

        private void createRatingTables(SQLiteConnection connection)
        {
            SQLiteCommand command = connection.CreateCommand();

            #region Create Nursing Unit table
            string commandText = @"CREATE TABLE IF NOT EXISTS Scores(
                Room            CHAR(10)        REFERENCES Room(Name),
                DischargeFrom   DATETIME,
                DischargeTo     DATETIME,
                Section         CHAR(30),
                Question        CHAR(200),
                Score           REAL,
                NumOfSurveys    INTEGER
            )";
            command.CommandText = commandText;
            command.ExecuteNonQuery();
            #endregion
        }

        /// <summary>
        /// The original date time format from excel looks like this:
        /// MM/DD/YYYY
        /// 
        /// We need to convert it to this:
        /// YYYY-MM-DD
        /// 
        /// </summary>
        /// <param name="oriDateTime"></param>
        /// <returns></returns>
        private string convertDateTime(string oriDateTime)
        {
            string[] separateDateAndTime = oriDateTime.Split(' ');
            string[] separateMDY = separateDateAndTime[0].Split('/');
            string MM = separateMDY[0];
            MM = MM.Length == 1 ? "0" + MM : MM;
            string DD = separateMDY[1];
            DD = DD.Length == 1 ? "0" + DD : DD;
            string YYYY = separateMDY[2];

            string finalString = YYYY + "-" + MM + "-" + DD;
            return finalString;
        }
        struct OneRow
        {
            public List<object> Room;
            public List<object> Patient;
            public List<object> Survey;
            public List<object> Comments;
        }        

        private string AddSingleQuoteIntoString(string value)
        {
            char[] tmpValue = value.ToCharArray();
            string newValue = "";

            // add additional single quote to prevent the exception in SQLite
            for (int k = 0; k < tmpValue.Count(); k++)
            {
                newValue = newValue + tmpValue[k];
                if (tmpValue[k] == '\'')
                    newValue = newValue + '\'';
            }

            return newValue;
        }

    }
}
