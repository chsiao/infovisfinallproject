﻿namespace ConvertToSQLite
{
    partial class Ribbon1 : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public Ribbon1()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SQLite = this.Factory.CreateRibbonTab();
            this.group1 = this.Factory.CreateRibbonGroup();
            this.button1 = this.Factory.CreateRibbonButton();
            this.MaxRow = this.Factory.CreateRibbonEditBox();
            this.group2 = this.Factory.CreateRibbonGroup();
            this.btConvertScore = this.Factory.CreateRibbonButton();
            this.txtSection = this.Factory.CreateRibbonEditBox();
            this.lbCurRow = this.Factory.CreateRibbonLabel();
            this.group3 = this.Factory.CreateRibbonGroup();
            this.btRoom = this.Factory.CreateRibbonButton();
            this.SQLite.SuspendLayout();
            this.group1.SuspendLayout();
            this.group2.SuspendLayout();
            this.group3.SuspendLayout();
            // 
            // SQLite
            // 
            this.SQLite.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
            this.SQLite.Groups.Add(this.group1);
            this.SQLite.Groups.Add(this.group2);
            this.SQLite.Groups.Add(this.group3);
            this.SQLite.Label = "SQLite";
            this.SQLite.Name = "SQLite";
            // 
            // group1
            // 
            this.group1.Items.Add(this.button1);
            this.group1.Items.Add(this.MaxRow);
            this.group1.Label = "Convert Comments";
            this.group1.Name = "group1";
            // 
            // button1
            // 
            this.button1.Label = "Convert Comments";
            this.button1.Name = "button1";
            this.button1.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btCommentConvert_Click);
            // 
            // MaxRow
            // 
            this.MaxRow.Label = "Max Row";
            this.MaxRow.Name = "MaxRow";
            this.MaxRow.Text = null;
            // 
            // group2
            // 
            this.group2.Items.Add(this.btConvertScore);
            this.group2.Items.Add(this.txtSection);
            this.group2.Items.Add(this.lbCurRow);
            this.group2.Label = "Rating1";
            this.group2.Name = "group2";
            // 
            // btConvertScore
            // 
            this.btConvertScore.Label = "Convert Scores";
            this.btConvertScore.Name = "btConvertScore";
            this.btConvertScore.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btConvertScore_Click);
            // 
            // txtSection
            // 
            this.txtSection.Label = "Section";
            this.txtSection.Name = "txtSection";
            this.txtSection.Text = null;
            // 
            // lbCurRow
            // 
            this.lbCurRow.Label = " ";
            this.lbCurRow.Name = "lbCurRow";
            // 
            // group3
            // 
            this.group3.Items.Add(this.btRoom);
            this.group3.Label = "Room";
            this.group3.Name = "group3";
            // 
            // btRoom
            // 
            this.btRoom.Label = "Convert Room Data";
            this.btRoom.Name = "btRoom";
            this.btRoom.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btRoom_Click);
            // 
            // Ribbon1
            // 
            this.Name = "Ribbon1";
            this.RibbonType = "Microsoft.Excel.Workbook";
            this.Tabs.Add(this.SQLite);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.Ribbon1_Load);
            this.SQLite.ResumeLayout(false);
            this.SQLite.PerformLayout();
            this.group1.ResumeLayout(false);
            this.group1.PerformLayout();
            this.group2.ResumeLayout(false);
            this.group2.PerformLayout();
            this.group3.ResumeLayout(false);
            this.group3.PerformLayout();

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab SQLite;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group1;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton button1;
        internal Microsoft.Office.Tools.Ribbon.RibbonEditBox MaxRow;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btConvertScore;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group2;
        internal Microsoft.Office.Tools.Ribbon.RibbonEditBox txtSection;
        internal Microsoft.Office.Tools.Ribbon.RibbonLabel lbCurRow;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group3;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btRoom;
    }

    partial class ThisRibbonCollection
    {
        internal Ribbon1 Ribbon1
        {
            get { return this.GetRibbon<Ribbon1>(); }
        }
    }
}
